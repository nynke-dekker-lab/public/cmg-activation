# CMG activation

Notebooks for the CMG activation project.

## Notebooks

- `motion_plots_figures.ipynb`: notebook for making motion related figures with WT CMG.
- `6A mutant motion plot_figures.ipynb`: notebook for making motion related figures with 6A mutant CMG.
- `Figure 1 plots.ipynb`: notebook for plotting figure 1
- `Figure S2 plots.ipynb`: notebook for plotting figure S2
- `Figure S4.ipynb`: notebook for plotting figure S4

Any data not included in `figure_data/` can be found at [https://doi.org/10.4121/19948253](https://doi.org/10.4121/19948253).

## Authors

- Edo van Veen: initial development of data analysis routines
- Daniel Ramirez Montero: performing data analysis and writing code for plotting
